let number = parseInt(prompt("Please enter a number:"));
console.log("The number you provided is " + number + ".");

for(let currentValue = number; currentValue >= 0; currentValue-=1) {
    if(currentValue <= 50){
        console.log("The current value is " + currentValue + ". Terminating the loop")
         break;
    }
    if(currentValue % 10 === 0) {
        console.log(currentValue + " is being skipped");
        continue;
    }
    if(currentValue % 5 === 0) {
        console.log(currentValue);
    }    

}

let word = "supercalifragilisticexpialidocious";
let consonants = "";

for (let i = 0; i < word.length; i++) {
    let char = word[i];
    if(char === 'a'|| char === 'e'|| char === 'i'|| char === 'o'|| char === 'u'){
        continue;
    }
    else{
        consonants += char;
    }
}
console.log(word);
console.log(consonants);


